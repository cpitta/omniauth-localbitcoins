require 'omniauth-oauth2'

module OmniAuth
  module Strategies
    class Localbitcoins < OmniAuth::Strategies::OAuth2
      option :name, 'localbitcoins'
      option :client_options, {
          :site => 'https://localbitcoins.com',
          :authorize_url => "/oauth2/authorize",
          :token_url => "/oauth2/access_token",
          :proxy => ENV['http_proxy'] ? URI(ENV['http_proxy']) : nil
      }

      uid { raw_info['username'] }

      info do
        {
            :id => raw_info['username'],
            :trading_partners_count => raw_info['trading_partners_count'],
            :feedbacks_unconfirmed_count => raw_info['feedbacks_unconfirmed_count'],
            :trade_volume_text => raw_info['trade_volume_text'],
            :has_common_trades => raw_info['has_common_trades'],
            :confirmed_trade_count_text => raw_info['confirmed_trade_count_text'],
            :blocked_count => raw_info['blocked_count'],
            :feedback_count => raw_info['feedback_count'],
            :url => raw_info['url'],
            :trusted_count => raw_info['trusted_count']
        }
      end

      extra do
        { :raw_info => raw_info }
      end

      def callback_url
        options[:redirect_uri] || (full_host + script_name + callback_path)
      end

      def raw_info
        @raw_info ||= MultiJson.load(access_token.get("/api/myself/?access_token=#{access_token.token}").body)['data']
      rescue ::Errno::ETIMEDOUT
        raise ::Timeout::Error
      end

    end
  end
end